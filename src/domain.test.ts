import { deepStrictEqual as assert } from "assert";
import adder from "./domain";

it(" adds", () => {
  const actual = adder(1, 2);
  assert(actual, 3);
});
