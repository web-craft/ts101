import * as Sentry from "@sentry/node";
import app from "./lib";
import { loadConfig, setupLogger } from "./config";

async function main(): Promise<void> {
  const config = await loadConfig();
  const logger = setupLogger(config.logLevel);
  logger.info(`Running with: Config ${JSON.stringify(config)} `);
  Sentry.init({
    dsn: config.sentryUrl,
  });
  app.listen(config.port, config.host);
}

main().then();
