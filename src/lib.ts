import express from "express";
import axios from "axios";
import adder from "./domain";

const app = express();

app.get("/", async (req, res) => {
  adder(1, 2);
  const { data } = await axios.get("https://httpbin.org/get");
  res.send(data.url);
});

export default app;
