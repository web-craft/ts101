import { deepStrictEqual as assert } from "assert";
import supertest, { SuperTest, Test } from "supertest";
import nock, { Body } from "nock";
import app from "../src/lib";
import { loadConfig } from "../src/config";

// eslint-disable-next-line max-params
async function init(
  url: string,
  path: string,
  body: Body,
  method: "get" | "post" | "put" | "delete" = "get",
  responseCode = 200
): Promise<SuperTest<Test>> {
  const config = await loadConfig();
  if (!config.e2e) {
    nock(url)[method](path).reply(responseCode, body);
  }
  return supertest(app);
}

describe("rest api", () => {
  it(" can get a url", async () => {
    const client = await init("https://httpbin.org", "/get", {
      url: "https://httpbin.org/get",
    });
    const res = await client.get("/");
    assert(res.text, "https://httpbin.org/get");
  });
});
